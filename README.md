# Docker Stack for Symfony 
    . Apache
    . php 7.2.3
    . Mariadb
    . phpmyadmin
    . maildev

## Environnement 
    . USER: dev
    . MYSQL_ROOT_PASSWORD: root
    . MYSQL_DATABASE: sf4
    . MYSQL_USER: sf4
    . MYSQL_PASSWORD: sf4
    . maildev_host: sf4_maildev

## Docker-compose 

Build image
```
docker-compose build
```

Deploy 
```
docker-compose up 
```

## Init Symfony 

Connect to container with dev
```
docker exec -it -u dev sf4_php bash
```

Go to home
```
cd /home/wwwroot/sf4
```

Install symfony
```
composer create-project symfony/skeleton symfony-app
```

Clean directory
```
cp -Rf /home/wwwroot/sf4/symfony-app/. .
rm -Rf /home/wwwroot/sf4/symfony-app
```

Case if permission denied
```
chmod 777 -R /var/cache/dev
chmod 777 -R /var/log
```

## Enjoy 

    . Your App -> localhost:80
    . phpmyadmin -> localhost:8888
    . maildeb -> localhost:8001